# wiki_scrape

A data scraping tool used for extracting names and important entities from specified wikipedia
websites. The tool is initially provided an entities and fetches the specified wikipedia web
page. Once fetched, the wikipedia page is extracted, parseed, reformatted, and relies on
basic natural language processing to determine important entities. This information
is return as a list of words saved to a CSV file, entitled by the keyword.

## Prerequisites

`Python3, nltk.corpus, nltk.tokenize, wikipedia, sys, nltk, nltk.download('stopwords'),
nltk.download('punkt'), nltk.download('averaged_perceptron_tagger'),
nltk.download('maxent_ne_chunker'),  nltk.download('words'), gensim, gensim.models, nameparser`

## Example

`python3 wiki_scrape.py <keyword>`

`python3 wiki_scrape.py "Donald Trump`
