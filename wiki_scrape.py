from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
import wikipedia
import sys
import nltk
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
import gensim
from gensim.models import Word2Vec

from nameparser.parser import HumanName

def get_human_names(text):
    tokens = word_tokenize(text)
    pos = nltk.pos_tag(tokens)
    sentt = nltk.ne_chunk(pos, binary = False)
    person_list = []
    person = []
    name = ""
    for subtree in sentt.subtrees(filter=lambda t: t.label() == 'PERSON'):
        for leaf in subtree.leaves():
            person.append(leaf[0])
        if len(person) > 1: #avoid grabbing lone surnames
            for part in person:
                name += part + ' '
            if name[:-1] not in person_list:
                person_list.append(name[:-1])
            name = ''
        person = []
    return (person_list)

def main(name):
    wikipedia.set_lang("en") 
    try:
        txt = wikipedia.summary(name)
    except:
        print("No Article Found")
        sys.exit()
    names = get_human_names(txt)
    with open(name + '.csv', 'w') as out_file:
        for name in names:
            first_last = HumanName(name).first + ' ' + HumanName(name).last
            out_file.write(first_last + '\n')
    
if __name__ == "__main__":
    if len(sys.argv) == 2:
        name = sys.argv[1]
        main(name)
    elif len(sys.argv) == 3:
        first_name = sys.argv[1]
        last_name = sys.argv[2]
        name = first_name + " " + last_name
        main(name)
    elif len(sys.argv) == 4:
        first = sys.argv[1]
        middle = sys.argv[2]
        last = sys.argv[3]
        name = first + " " + middle + " " + last
        main(name)
    else:
        print("python wiki_scrape.py <name>")
